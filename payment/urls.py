from django.conf.urls import url
from . import views


urlpatterns = [
	#Descarga de fotos
    url(r'^process/', views.payment_process, name='process'),
    url(r'^done/', views.payment_done, name='done'),
    url(r'^canceled/', views.payment_canceled, name='canceled'),
    url(r'^download/', views.payment_download, name='download'),
    
    # Excursionses
    url(r'^processexcursion/', views.payment_excursion, name='payexcur'),
    url(r'^done_excursion/', views.payment_done_excursion, name='done_excursion'),
    url(r'^canceled_excursion/', views.payment_canceled_excursion, name='canceled_excursion'),

]