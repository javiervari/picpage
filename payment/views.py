# -*- coding: utf-8 -*-
import json, zipfile, os
from wsgiref.util import FileWrapper
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, render_to_response
from paypal.standard.forms import PayPalPaymentsForm
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect


from download.models import *
from excursion.models import *


# Downloads
@csrf_exempt
def payment_download(request):
	backend_dict = request.session['backend_context'] #recibo los parametros desde la cache
	with zipfile.ZipFile('MyTailorToursPhotos.zip', 'w') as export_zip:
		for image in backend_dict['photos_selected']:
			image = image.split('_indicador_')[0]
			export_zip.write(image, os.path.basename(image))
	wrapper = FileWrapper(open('MyTailorToursPhotos.zip', 'rb'))
	content_type = 'application/zip'
	content_disposition = 'attachment; filename=MyTailorToursPhotos.zip'
	response = HttpResponse(wrapper, content_type=content_type)
	response['Content-Disposition'] = content_disposition
	return response


@csrf_exempt
def payment_done(request):
	backend_dict = request.session['backend_context'] #recibo los parametros desde la cache
	response = render_to_response('payment/download/done.html', ) #render done html
	response['Refresh'] = "2;url=/payment/download" #mando a actualizar a los 2 seg
	request.session['backend_dict'] = backend_dict #almaceno el contexto en cache

	return response


	

@csrf_exempt
def payment_canceled(request):
	return render(request, 'payment/download/canceled.html')


def payment_process(request):

	backend_dict = request.session['backend_context'] #recibo los parametros desde la cache
	
	total_price = int(backend_dict['total_price']) if backend_dict['total_price'] else 0
	
	if backend_dict['amount_free_photos']:
		amount_free_photos=int(backend_dict['amount_free_photos'])
	else:
		amount_free_photos=0

	if backend_dict['amount_paid_photos']:
		amount_paid_photos=int(backend_dict['amount_paid_photos'])
	else:
		amount_paid_photos=0

	item_name = '{}Pics+{}Free'.format(amount_paid_photos, amount_free_photos)
	backend_dict['detail'] = item_name #para mi

	invoice_id = backend_dict['invoice_id'] #para paypal


	paypal_dict = {
		'business': settings.PAYPAL_RECEIVER_EMAIL,
		'amount': total_price,
		'item_name': item_name,
		'invoice': invoice_id, #ID de Factura
		'currency_code': 'USD',
		'notify_url':'http://{}{}'.format(request.get_host(), reverse('paypal-ipn')),
		'return_url':'http://{}{}'.format(request.get_host(), reverse('payment:done')),
		'cancel_return':'http://{}{}'.format(request.get_host(), reverse('payment:canceled')),
	}
	form = PayPalPaymentsForm(initial=paypal_dict)
	context = {'form': form,
				'backend_dict': backend_dict
				}

	request.session['backend_dict'] = backend_dict #almaceno el context en cache


	return render(request, 'payment/download/process.html', context)




# Excursions

def payment_excursion(request):
	backend_dict = request.session['backend_context'] #recibo los parametros desde la cache

	item_name = "{}&{}&{}&{}&{}&{}".format(
		"{} {}".format(backend_dict['name'], backend_dict['lastname']),
		backend_dict['excursion_name'],
		backend_dict['date'],
		backend_dict['turn'],
		backend_dict['id_excur'],
		backend_dict['amount_people'],
		)

	paypal_dict = {
		'business': settings.PAYPAL_RECEIVER_EMAIL,
		'amount': int(backend_dict['total_to_pay']),
		'item_name': item_name ,
		'invoice': backend_dict['invoiceid'], #ID de Factura
		'currency_code': 'USD',
		'notify_url':'http://{}{}'.format(request.get_host(), reverse('paypal-ipn')),
		'return_url':'http://{}{}'.format(request.get_host(), reverse('payment:done_excursion')),
		'cancel_return':'http://{}{}'.format(request.get_host(), reverse('payment:canceled_excursion')),
	}
	form = PayPalPaymentsForm(initial=paypal_dict)
	
	context = {'form': form,
				'backend_dict': backend_dict
				}

	request.session['backend_dict'] = backend_dict #almaceno el context en cache


	return render(request, 'payment/excursion/process.html', context)



@csrf_exempt
def payment_done_excursion(request):
	try:
		backend_dict = request.session['backend_context'] #recibo los parametros desde la cache
		id_excur = Excursion.objects.get(id=backend_dict['id_excur'])

		tourist_obj = Tourist(name = backend_dict['name'],
						lastname = backend_dict['lastname'],
						email = backend_dict['email'],
						phone = backend_dict['phone'],
						amount_people = backend_dict['amount_people'],
						hotel = backend_dict['hotel'],
						comment = backend_dict['comment'],
						excursion = id_excur
						)

		tourist_obj.save()
		del request.session['backend_context']
		return render(request, 'payment/excursion/done.html')
	
	except Exception as e:
		return redirect ('/')
	


	



@csrf_exempt
def payment_canceled_excursion(request):
	return render(request, 'payment/excursion/canceled.html')
