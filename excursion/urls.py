from django.conf.urls import url
from excursion.views import (initial_view, 
							order_view, 
							ajax_query)


urlpatterns = [
    url(r'^$', initial_view, name='initial'),
    url(r'^order$', order_view, name='order'),
    url(r'^ajax$', ajax_query, name='ajax'),

]