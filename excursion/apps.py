# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class ExcursionConfig(AppConfig):
    name = 'excursion'
    def ready(self):
    	import excursion.signals
