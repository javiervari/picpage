from __future__ import unicode_literals

import json
from datetime import datetime

from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

from django.core.serializers.json import DjangoJSONEncoder

from .models import Excursion, Tourist, Hotel
from .forms import TouristForm


def initial_view(request):
	if request.method == 'POST':
		for k,v in request.POST.items():
			if k != 'csrfmiddlewaretoken':
				excursion_name = k

		qs_available_dates = Excursion.objects\
			.filter(name__iexact=excursion_name)\
			.values_list('date', flat=True).order_by('date')

		available_dates = json.dumps([str(date) for date in qs_available_dates])

		price = Excursion.objects\
					.filter(name__iexact=excursion_name)\
					.values_list('price',flat=True)\
					.distinct()[0]

		form = TouristForm(
			initial={'excursion':excursion_name,'price':price},
			hotels=Hotel.objects.all().values_list('name',flat=True)
			)


		context = {'excursion_name': excursion_name,
					'price': price,
					'available_dates':available_dates,
					'start': qs_available_dates[0].strftime("%d-%m-%Y"),
					'end': qs_available_dates.reverse()[0].strftime("%d-%m-%Y"),
					'form':form
					}
		return render(request, 'form_excursion.html', context)


	else:
		context = {'excursions':[]}
		for i in Excursion.objects.all().distinct().values('name','detail'):
			dateqs, priceqs = [Excursion.objects\
									.filter(name = i['name'])\
									.values_list('date', flat=True)\
									.distinct() , 
							Excursion.objects\
									.filter(name = i['name'])\
									.values_list('price', flat=True)\
									.distinct()[0]
									]

			context['excursions'].append({'name':i['name'],
										   'detail':i['detail'],
										   'date':dateqs,
										   'price':priceqs
										   })


		return render(request, "excursion.html", context)


@csrf_exempt
def order_view(request):
	if request.method == 'POST':

		form = TouristForm(data=request.POST, hotels=None)

		if form.is_valid():
			data = form.cleaned_data
			print(data)

			invoiceid = '{} {} paid:{}'.format(data['excursion'],
										data['lastname'],
										datetime.now().strftime('%Y-%m-%d at %H:%M:%S'))

			excur_obj = Excursion.objects\
				.get(name__iexact=data['excursion'],
					date=data['date'],
					turn=data['turn'])


			contexto = {'excursion_name': data['excursion'],
						'name': data['name'],
						'lastname': data['lastname'],
						'email': data['email'],
						'phone': data['phone'],
						'date':data['date'], 
						'turn':data['turn'],
						'hotel': data['hotel'],
						'amount_people': data['amount_people'],
						'comment': data['comment'],
						'total_to_pay': float(data['price']) * float(data['amount_people']),
						'invoiceid':invoiceid,
						'id_excur': excur_obj.id
						}
			
			request.session['backend_context'] = contexto #almaceno el contexto en cache

			return redirect(reverse('payment:payexcur')) #llamo a la view de pago
		
		else:
			print('no es valido', form.errors)

			excursion_name = request.POST.get('excursion')
			price = request.POST.get('price')
			qs_available_dates = Excursion.objects\
				.filter(name__iexact=excursion_name)\
				.values_list('date', flat=True).order_by('date')

			available_dates = json.dumps([str(date) for date in qs_available_dates])
			form = TouristForm(data=request.POST,
				initial={'excursion':excursion_name,'price':price},
				hotels=Hotel.objects.all().values_list('name',flat=True)
				)

			context = {'excursion_name': excursion_name,
					'price': price,
					'available_dates':available_dates,
					'start': qs_available_dates[0].strftime("%d-%m-%Y"),
					'end': qs_available_dates.reverse()[0].strftime("%d-%m-%Y"),
					'form':form
					}
			return render(request, 'form_excursion.html', context)


	else:
		return redirect('/')





def ajax_query(request):
	
	if request.GET.get('solicitud')=='turn':
		selected_date = request.GET.get('selected_date', None)
		excursion = request.GET.get('excursion', None)
		qs_turns = Excursion.objects\
					.filter(name__iexact=excursion, date=selected_date)\
					.values('turn')\
					.order_by('turn')

		return HttpResponse(json.dumps(list(qs_turns), cls=DjangoJSONEncoder))

	elif request.GET.get('solicitud')=='available':
		selected_date = request.GET.get('selected_date', None)
		excursion = request.GET.get('excursion', None)
		turn = request.GET.get('turn', None)

		qs_available_coupons = Excursion.objects\
						.filter(name__iexact=excursion,
								date=selected_date,
								turn=turn)\
						.values_list('available',flat=True)

		return HttpResponse(json.dumps(qs_available_coupons[0], cls=DjangoJSONEncoder))



