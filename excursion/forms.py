from datetime import datetime
from django import forms
from excursion.models import Tourist, Hotel


def set_field_html_name(cls, new_name):
    """
    Cambia el nombre del attr 'name', ya que el objeto forms.Form
    le coloca por default el nombre del campo.
    """
    old_render = cls.widget.render
    def _widget_render_wrapper(name, value, attrs=None):
        return old_render(new_name, value, attrs)

    cls.widget.render = _widget_render_wrapper



class TouristForm(forms.Form):
	name = forms.CharField(
		label = 'Name', 
		max_length=100,
		widget=forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter name...'}
			)
		)
	lastname = forms.CharField(
		label = 'Last Name', 
		max_length=100,
		widget=forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter last name...'}
			)
		)
	email = forms.EmailField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'email@domain.com'}
			)
		)
	phone = forms.CharField(max_length=20, 
		widget=forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter a cellphone number...'}
			)
		)


	excursion = forms.CharField(max_length=100,
		widget=forms.TextInput(attrs={
			'class': 'form-control',
			'readonly':True}
			)
		)

	price = forms.FloatField(
		widget=forms.HiddenInput(),
		)

	hotel = forms.CharField()

	amount_people = forms.IntegerField()

	date = forms.DateField(
		widget=forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter a date...',
			'id': 'fecha'}
			))
	# set_field_html_name(date, 'daterange')#cambiando el valor de 'name'

	turn = forms.CharField()



	comment = forms.CharField(
		label = 'Comments', 
		max_length=300,
		widget=forms.Textarea(attrs={
			'rows':3, 'style':'resize:none',
			'class':'form-control cls-input',
			'placeholder': 'Enter your comment...'})
		)

	def __init__(self, hotels, *args, **kwargs):
		super(TouristForm, self).__init__(*args, **kwargs)
		# self.fields['turn'] = forms.ChoiceField(
		# 	choices=[(turn, turn.strftime('%I:%M %p')) for turn in available_turns]
		# 		)
		if hotels:
			self.fields['hotel'] = forms.ChoiceField(
				choices=hotels)


	def clean_date(self):
		selected_date = self.cleaned_data['date']
		return selected_date.strftime("%Y-%m-%d")

		
