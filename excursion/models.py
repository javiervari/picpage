# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime, time

from django.db import models

from django.conf import settings

HOUR_CHOICES = [(time(hour=x), '{:02d}:00'.format(x)) for x in range(0, 24)]

class Excursion(models.Model):
	name = models.CharField(max_length=100)
	date = models.DateField()
	turn = models.TimeField(choices=HOUR_CHOICES)
	detail = models.TextField()
	available = models.IntegerField()
	price = models.FloatField()

	def __unicode__(self):
		unicode_str = "{} at {} {}".format(self.name, self.date, self.turn.strftime("%I %p"))
		return unicode_str


class Tourist(models.Model):
	name = models.CharField(max_length=100)
	lastname = models.CharField(max_length=100)
	email = models.EmailField(max_length=100)
	phone = models.CharField(max_length=20)
	amount_people = models.IntegerField()
	hotel = models.CharField(max_length=50)
	comment = models.TextField()
	excursion = models.ForeignKey('Excursion', on_delete=models.CASCADE)

	def __unicode__(self):
		unicode_str = "{} {} {} {} {}".format(self.name, 
											self.lastname, 
											self.email, 
											self.phone, 
											self.excursion
											)
		return unicode_str


class Hotel(models.Model):
	name = models.CharField(max_length=100)


class Reserve(models.Model):
	tourist = models.CharField(max_length=100)



    