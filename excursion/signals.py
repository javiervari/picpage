from django.shortcuts import get_object_or_404
from paypal.standard.ipn.signals import valid_ipn_received
from django.dispatch import receiver
from django.conf import settings
from django.core.mail import send_mass_mail

from django.db.models import F

from .models import Excursion, Tourist, Hotel

 
 
@receiver(valid_ipn_received)
def payment_notification(sender, **kwargs):
	"""NOMBRES DE LAS VARIABLES EN 
	Https://developer.paypal.com/webapps/developer/docs/classic/ipn/integration-guide/IPNandPDTVariables/#mass-pay-variables

	INFO DE SIGNALS EN
	https://overiq.com/django-paypal-integration-with-django-paypal/

	para correr a modo de prueba es 
	./ngrok http 8000
	y correr el runserver normal

	"""
	ipn = sender	
	if ipn.payment_status == 'Completed':
		# payment was successful
		# for key, val in ipn.__dict__.items():
		# 	print(key,"---->",val)

		sender_email = settings.EMAIL_HOST_USER

		mytailor_email = settings.MYTAILOR_EMAIL

		client_email = ipn.payer_email
		cliente, excursion, fecha, turno, id_excur, cantidad_personas = ipn.item_name.split('&')
		precio = ipn.mc_gross
		moneda = ipn.currency_code
		id_comprobante = str(ipn.txn_id)

		#Actualizando cantidad de cupos disponibles
		Excursion.objects.filter(id=id_excur)\
			.update(available=F('available')-int(cantidad_personas))


		mytailor_title = '{} - {} Excursion Reserved'.format(id_comprobante, excursion)

		mytailor_body = """Se ha procesado exitosamente el pago del cliente: {0}

		DETALLES DE LA RESERVA
		Excursion: {1}
		Fecha y Hora de la Excursion: {2} at {3}
		Precio: {4} {5}
		Id Transaccion PayPal: {6}
		""".format(
			cliente,
			excursion,
			fecha,
			turno,
			moneda, precio,
			id_comprobante
			)

		client_title = '{} Excursion Reserved'.format(excursion)

		client_body = """Buenas tardes estimad@ {0}. 
		Su pago para la excursion {1} ha sido procesado exitosamente.
		Detalles:
			Fecha y Hora de la Excursion: {2} at {3}
			Precio de la Excursion: {4} {5}
			Id Transaccion PayPal: {6}

		Gracias por preferirnos
		Para mas informacion comuniquese a los numeros +12345678 o al correo electronico asd@asd.com


		Nota: No responda este correo
		""".format(
			cliente,
			excursion,
			fecha,
			turno,
			moneda, precio,
			id_comprobante
			)


		# datatuple = (
		# 	(client_title, client_body, sender_email, [client_email,]),
		# 	(mytailor_title, mytailor_body, sender_email, [mytailor_email,]),
		# 	) DESBLOQUER POST TEST

		datatuple = (
			(client_title, client_body, sender_email, ['javiervari@gmail.com']),
			(mytailor_title, mytailor_body, sender_email, ['javiervari@gmail.com']),
			)
		send_mass_mail(datatuple)
		print("Emails sended successfully")

