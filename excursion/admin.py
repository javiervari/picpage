# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Excursion, Tourist, Hotel


# Register your models here.

class ExcursionAdmin(admin.ModelAdmin):

	def available_coupons(self, obj):
		if (0 < obj.available <= 5):
			return '<div style="color:orange; font-size: 20px; font-weight: bold;">{}</div>'.format(obj.available)
		elif (obj.available > 5):
			return '<div style="color:green; font-size: 20px; font-weight: bold;">{}</div>'.format(obj.available)
		else:
			return '<div style="color:red; font-size: 20px; font-weight: bold;">{}</div>'.format(obj.available)

	available_coupons.allow_tags = True
	list_display = ('name', 'date', 'turn', 'available_coupons')



class TouristAdmin(admin.ModelAdmin):
	list_display = ('name','lastname','email','amount_people','excursion')



class HotelAdmin(admin.ModelAdmin):
	list_display = ('name',)




admin.site.register(Excursion, ExcursionAdmin)
admin.site.register(Tourist, TouristAdmin)
admin.site.register(Hotel, HotelAdmin)
