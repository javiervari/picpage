# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import FileExtensionValidator

from django.db.models.signals import post_delete
from django.dispatch import receiver


class Pic(models.Model):
	date = models.DateField()
	image = models.ImageField(upload_to='pictures', 
							validators=[FileExtensionValidator(['jpg'])])
	verification_code = models.CharField(max_length=5)
	is_free = models.BooleanField(default=False)

@receiver(post_delete, sender=Pic) #Eliminar archivos de media
def submission_delete(sender, instance, **kwargs):
    instance.image.delete(False) 



class Downloader(models.Model):
	date = models.DateField(auto_now_add=True)
	name = models.CharField(max_length=100)
	lastname = models.CharField(max_length=100)
	email = models.EmailField(max_length=100)
	comment = models.TextField()


