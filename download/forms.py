from django import forms
from download.models import *

class DownloadPicForm(forms.Form):
	name = forms.CharField(
		label = 'Name', 
		max_length=200,
		widget=forms.TextInput(attrs={
			'class':'form-control cls-input',
			'placeholder': 'Name...'})
		)

	lastname = forms.CharField(
		label = 'Last Name', 
		max_length=200,
		widget=forms.TextInput(attrs={
			'class':'form-control cls-input',
			'placeholder': 'Last Name...'})
		)

	email = forms.EmailField(
		widget=forms.TextInput(attrs={
			'class':'form-control cls-input',
			'placeholder': 'email@domain.com'}))

	comment = forms.CharField(
		label = 'Comments', 
		max_length=300,
		widget=forms.Textarea(attrs={
			'rows':3, 'style':'resize:none',
			'class':'form-control cls-input',
			'placeholder': 'Enter your comment...'})
		)

	photo_date = forms.DateField(widget=forms.TextInput(attrs={
			'class':'form-control cls-input',
			'readonly':True
			
			}
		)
	)

	code = forms.CharField(
		label = "Day Code", 
		max_length=5,
		widget=forms.TextInput(attrs={
			'class':'form-control cls-input-code',
			'placeholder': '5 digits code'})
		)
	# def __init__(self, *args, **kwargs):
	# 	super(DownloadPicForm, self).__init__(*args, **kwargs)
	# 	for visible in self.visible_fields():
	# 		visible.field.widget.attrs['class'] = 'form-control cls-input'
	

	def clean_code(self):
		code_inserted = self.cleaned_data['code']  
		verification_code = Pic.objects.filter(date=self.cleaned_data['photo_date'])\
										.values_list('verification_code', flat=True)\
										.distinct()[0]

		if code_inserted != verification_code:
			raise forms.ValidationError("Invalid code! The code entered and code of this day doesn't match")
		return code_inserted




