# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Pic, Downloader

import random, string


def free(modeladmin, request, queryset):
    for q in queryset:
        Pic.objects.filter(id=q.id).update(is_free=not(q.is_free))

# admin.site.disable_action('delete_selected') #disable Delete Option
free.short_description = "Fotos Gratis Activar/Desactivar"

class PicAdmin(admin.ModelAdmin):
    exclude = ['image', 
                'verification_code', 
                'is_free'] #excluding original form and verification_code

    list_display = ('id', 
                    'is_free', 
                    'image', 
                    'date', 
                    'verification_code') #lists image and date in DjangoAdmin

    date_hierarchy = 'date' #crumbs
    actions = [free, ] #Action List

    def random_code(self,fecha):
        """Este metodo genera codigos de 5 digitos random 
        si no existen fotos en esa fecha,
        de lo contrario retorna el codigo perteneciente a la fecha"""
    	
        try:
            code = Pic.objects.filter(date=fecha).values_list('verification_code', flat=True)[0]
            return code
        except:
            numbers = string.digits
            letters=string.ascii_uppercase
            return ''.join(random.choice(numbers+letters) for i in range(5))

    def save_model(self, request, obj, form, change):
        code = self.random_code(request.POST.get('date'))
        print("SAVE_MODEL", request.POST)

    	for obj in request.FILES.getlist('pictures'):
    		pic = Pic(image=obj, date=request.POST.get('date'), verification_code=code, is_free=False)
    		pic.save()



class DownloaderAdmin(admin.ModelAdmin):
    list_display = ('id', 
                    'date', 
                    'name', 
                    'lastname') #lists image and date in DjangoAdmin


admin.site.register(Pic, PicAdmin)

admin.site.register(Downloader, DownloaderAdmin )