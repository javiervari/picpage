# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json, zipfile, os
import ntpath
from datetime import datetime

from django.conf import settings
from wsgiref.util import FileWrapper
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from download.forms import DownloadPicForm
from download.models import Pic, Downloader

from PIL import Image

from paypal.standard.forms import PayPalPaymentsForm



def marked(absolute_path, name_img):
	watermark_img_path = '{}/images/personal/snippets/marcadeagua.png'.format(settings.STATICFILES_DIRS[0])
	base_image = Image.open(absolute_path)
	watermark = Image.open(watermark_img_path)
	width, height = base_image.size
	pos_X_mark, pos_Y_mark = int(0), int(height/2)
	# pos_X_mark, pos_Y_mark = int(width/4), int(height/4)


	transparent = Image.new('RGB', (width, height), (0,0,0))
	transparent.paste(base_image, (0,0))
	transparent.paste(watermark, (pos_X_mark, pos_Y_mark), mask=watermark)
	path_img_marked = '{}/marked/{}'.format(settings.MEDIA_ROOT, name_img)
	transparent.save(path_img_marked)
	return path_img_marked


def initial_view(request):
	"""
	If request is POST: 
		A form is returned with the photo_date as Initial Value
	If request is GET: 
		A query is made to DB to get Availables Dates and set it in Calendar
	"""
	if request.method == "POST":
		photo_date = request.POST.get('daterange')
		download_form = DownloadPicForm(initial={'photo_date':photo_date})
		context = {'download_form':download_form,
					'photo_date':photo_date
					}
		return render(request, 'order.html', context)


	else:
		available_dates = []

		queryset_dates = Pic.objects.all()\
							.values_list('date',flat=True).order_by('date')\
							.distinct()

		try:
			start = queryset_dates[0].strftime("%d-%m-%Y")
			end = queryset_dates.reverse()[0].strftime("%d-%m-%Y")
		except Exception as e:
			start=None
			end=None
		

		for f in queryset_dates:
			available_dates.append(str(f))
		available_dates = json.dumps(available_dates)


		contexto = {'available_dates':available_dates,
					'start': start,
					'end':end,
					}

		return render(request, "initial.html", contexto)


def order_view(request):
	"""
	If request is POST: 
		The form is validated, storage the Downloader and pass photo path and is_free attr.
	If request is GET: 
		redirect to initial template
	"""
	if request.method == "POST":
		form = DownloadPicForm(request.POST)
		if form.is_valid():
			data = form.cleaned_data
			downloader = Downloader(date=data['photo_date'],
									name=data['name'],
									lastname=data['lastname'],
									email=data['email'],
									comment=data['comment']
									)
			downloader.save()

			photos = Pic.objects.filter(date=data['photo_date'])\
								.values('image', 'is_free')

			context = {'photo_date':data['photo_date'],
						'photos': photos
						}

			request.session['lastname'] = data['lastname'] #almaceno el apellido en cache


			return render(request, 'photos.html', context)
			

		else:
			context = {'download_form':form,
						'photo_date': request.POST['photo_date']}
			return render(request, 'order.html', context)

	else:
		return redirect('/')


@csrf_exempt
def photos_view(request):
	"""
	If request is POST: 
		If is free download, download the photos with markwater
		If is full download, redirect to PayPal
	If request is GET:
		redirect to initial template
	"""
	if request.method == 'POST':
		if 'free' in request.POST.keys(): #Presiono el boton de descarga gratis
			with zipfile.ZipFile('MyTailorToursPhotos.zip', 'w') as export_zip:
				for key, value in request.POST.items():
					if key != 'free': #obvia name del boton
						path , is_free = key.split('_indicador_')
						img_path="{}/{}".format(settings.MEDIA_ROOT,path)

						if int(is_free):
							export_zip.write(img_path, os.path.basename(img_path))
						else:
							img_marked = marked(absolute_path=img_path, name_img=ntpath.basename(path))
							export_zip.write(img_marked, os.path.basename(img_marked))


			wrapper = FileWrapper(open('MyTailorToursPhotos.zip', 'rb'))
			content_type = 'application/zip'
			content_disposition = 'attachment; filename=MyTailorToursPhotos.zip'
			response = HttpResponse(wrapper, content_type=content_type)
			response['Content-Disposition'] = content_disposition

			#antes de retornar el .zip borro todas las fotos almacenadas
			#en el directorio /media/marked/ ya que las imagenes guardadas 
			#en esta carpeta son temporales

			folder_temp = '{}/marked'.format(settings.MEDIA_ROOT)
			for img in os.listdir(folder_temp):
				img_to_remove_path="{}/{}".format(folder_temp,img)
				os.remove(img_to_remove_path)
			return response
			
		else: #presiono el boton de Full download
			invoice_id = 'DOWNLOADPHOTO_{}{}'.format(request.session['lastname'],\
									datetime.now().strftime('%Y%m%d_%H%M%S'))
			pic_paths = []
			amount_free_photos=0
			amount_paid_photos=0
			for key, value in request.POST.items():
				if key != 'pay':
					path , is_free = key.split('_indicador_')
					if int(is_free):
						amount_free_photos+=1
					else:
						amount_paid_photos+=1

					pic_paths.append(path)

			photos_selected=[]
			for key, val in request.POST.items():
				if key != 'pay':
					img_path="{}/{}".format(settings.MEDIA_ROOT,key)
					photos_selected.append(img_path)


			context = {'amount': len(pic_paths),
						'amount_free_photos': amount_free_photos,
						'amount_paid_photos': amount_paid_photos,
						'unit_price': settings.PRICE_P_PHOTO,
						'total_price': (amount_paid_photos*settings.PRICE_P_PHOTO),
						'pics':pic_paths, 
						'invoice_id':invoice_id,
						'photos_selected': photos_selected
						}
			request.session['backend_context'] = context #almaceno el contexto en cache

			return redirect(reverse('payment:process')) #llamo a la view de pago
			
						

	else:
		return redirect('/')


	
