var s,l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,g1,g2,g3,g4,circle;
s = Snap('#waves');
l1 = s.select('#layer-1');
l2 = s.select('#layer-2');
l3 = s.select('#layer-3');
l4 = s.select('#layer-4');
l5 = s.select('#layer-5');
l6 = s.select('#layer-6');
l7 = s.select('#layer-7');
l8 = s.select('#layer-8');
l9 = s.select('#layer-9');
l10 = s.select('#layer-10');
l11 = s.select('#layer-11');
l12 = s.select('#layer-12');

//circle = s.circle(150, 50, 100);
//circle.filter(Snap.filter.blur(10,10));

//g1 = s.group(l1,l5,l10);
//g2 = s.group(l2,l6,l11);
//l1.animate({x: 100}, 7000);
//l1.attr({transform: 't0 150,r3'})

/*l1.animate({transform: 't0 150'},400,function(){
  l1.animate({transform: 't0 0'},500)
})*/
g1 = s.group(l12,l11,l10,l9,l8,l7,l6,l5,l4,l3,l2,l1);

function waves(obj,x,length){
  var start = obj.transform();
  obj.animate({transform: 't'+x+' 0' },length,function(){
    obj.animate({transform: start},length,function(){
      waves(obj,x,length);
    })
  })
};

waves(l12,-40,2000);
waves(l11,-60,4000);
waves(l10,-10,1200);
waves(l9,-70,3000);
waves(l8,-20,2000);
waves(l7,-10,1200);
waves(l6,-40,2000);
waves(l5,-20,2000);
waves(l4,-70,1200);
waves(l3,-40,2000);
waves(l2,-60,2000);
waves(l1,-10,1200);
