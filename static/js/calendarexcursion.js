function ajaxFunctionTurnos(data){
	$.ajax({
			url: 'ajax',
			data: data,
			dataType: 'json',
			success: function (data) {
				$('#turn').find('option').not(':first').remove()
			    
				$.each( data, function( key, value ) {
				  var turno = Object.values(value).toString();
				  var turno_formateado = moment(turno,'hh:mm:ss').format('LT')
				  $('#turn').find('option').end().append('<option value="'+turno+'">'+turno_formateado+'</option>')
				  
				});
				
			}
		});
} 






function ajaxFunctionPersonas(data) {
	$.ajax({
		url: 'ajax',
		data:data,
		dataType: 'json',
		success: function (data) {
			$('#amount').find('option').not(':first').remove()

			$.each(new Array(data),function(n){
				var m = n+1;
				$('#amount').find('option').end().append('<option value="'+m+'">'+m+' persons</option>')

			});
			
		}
	})
}





$(document).ready(function() {
	

	var dates = JSON.parse(available_dates.replace(/&quot;/g,'"'));

	// Calendario y Selecciona Turnos al seleccionar una fecha
	$(function() {
		$('input[name="date"]').daterangepicker({
			opens: 'center',
		    drops: "down",
		    singleDatePicker: true,
		    minDate:start,
		    maxDate:end,
		    autoUpdateInput: false, //disable default date
		    locale: {
		    	format: 'DD-MM-YYYY'
		    },
		    isInvalidDate: function(date){
				return dates.indexOf(date.format('YYYY-MM-DD'))<false;
		    }
		}, function(start, end, label){
			let selected_date = $('input[name="date"]').val(start.format('YYYY-MM-DD'))
			$('#turn').find('option').not(':first').remove()
			$('#amount').find('option').not(':first').remove()

			$("#turn").val($("#turn option:first").val());
			$("#amount").val($("#amount option:first").val());

			ajaxFunctionTurnos({
				'solicitud':'turn',
				'selected_date': selected_date.val(),
				'excursion': $("#id_excursion").val()
			});
		});
	});


	$('#turn').on('change', function() {
	// Selecciona cantidad de personas disponibles segun la excursion
		ajaxFunctionPersonas({
			'solicitud':'available',
		  	'selected_date':$('input[name="date"]').val(),
		  	'excursion': $("#id_excursion").val(),
		  	'turn': $(this).val()
		  });


	});




	$('#amount').on('change', function() {
	//Modifica el valor del span #totaltopay, PRECIO * CANTIDAD DE PERSONAS
		var n =	$(this).val();
		$('#totaltopay').text('$ '+(n*price));
	});



});



