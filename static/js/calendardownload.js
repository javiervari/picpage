$(document).ready(function() {

	var dates = JSON.parse(available_dates.replace(/&quot;/g,'"'));
	$(function() {
		$('input[name="daterange"]').daterangepicker({
			opens: 'center',
		    drops: "down",
		    singleDatePicker: true,
		    minDate:start,
		    maxDate:end,
		    autoUpdateInput: false,
		    locale: {
		    	format: 'DD-MM-YYYY'
		    },
		    isInvalidDate: function(date){
				return dates.indexOf(date.format('YYYY-MM-DD'))<false;
		    }
		}, function(start, end, label){
			$('input[name="daterange"]').val(start.format('YYYY-MM-DD'))
			$("#form").submit();
		});
	});




});


